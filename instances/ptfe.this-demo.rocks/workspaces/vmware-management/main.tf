provider "vsphere" {
  # If you have a self-signed cert
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.dc_name
}

data "vsphere_compute_cluster" "compute_cluster" {
  name          = var.compute_cluster_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore_cluster" "datastore_cluster" {
  name          = var.datastore_cluster_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_resource_pool" "cpolansky_pool" {
  name                    = "cpolansky-pool"
  parent_resource_pool_id = data.vsphere_compute_cluster.compute_cluster.resource_pool_id
  tags                    = [vsphere_tag.tag_cpolansky.id]
}

data "vsphere_network" "network" {
  name          = var.vm_network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = "Ubuntu"
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "test-vm" {
  name                 = "rguthrie-test-vm"
  resource_pool_id     = data.vsphere_compute_cluster.compute_cluster.resource_pool_id
  datastore_cluster_id = data.vsphere_datastore_cluster.datastore_cluster.id
  folder               = "cpolansky"
  tags                 = [vsphere_tag.tag_cpolansky.id]

  num_cpus = 1
  memory   = 4096
  guest_id = data.vsphere_virtual_machine.template.guest_id

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  disk {
    label            = "disk0"
    size             = data.vsphere_virtual_machine.template.disks.0.size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = "cpolansky-rguthrie-test-vm"
        domain    = "hashicorp.com"
      }

      network_interface {}
    }
  }
}
