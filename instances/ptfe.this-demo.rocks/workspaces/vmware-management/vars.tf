variable "dc_name" {
  default = "PacketDatacenter"
  type    = string
}

variable "compute_cluster_name" {
  default = "MainCluster"
  type    = string
}

variable "datastore_cluster_name" {
  default = "DatastoreCluster"
  type = string
}

variable "vm_network_name" {
  default = "VM Network"
  type = string
}
