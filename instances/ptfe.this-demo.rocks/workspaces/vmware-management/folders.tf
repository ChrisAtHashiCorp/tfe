resource "vsphere_folder" "cpolansky" {
  datacenter_id = data.vsphere_datacenter.dc.id
  path          = "cpolansky"
  type          = "vm"
  tags          = [vsphere_tag.tag_cpolansky.id]
}
